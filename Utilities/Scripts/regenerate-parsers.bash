#!/usr/bin/env bash

set -e

forced=1
if [[ "${1}" = "make" ]]; then
    forced=0
fi

pushd "${BASH_SOURCE%/*}/../../Source/LexerParser" > /dev/null

for parser in            \
    CommandArgument     \
    DependsJava         \
    Expr                \
    Fortran
do
    in_file=cm${parser}Parser.y
    cpp_file=cm${parser}Parser.cxx
    h_file=cm${parser}ParserTokens.h
    prefix=cm${parser}_yy

    if [[ (${in_file} -nt ${cpp_file}) || (${in_file} -nt ${h_file}) || (${forced} -gt 0) ]]; then
        echo "Generating Parser ${parser}"
          bison --yacc --name-prefix=${prefix} --defines=${h_file} -o${cpp_file}  ${in_file}
#        sed -i 's/\s*$//'                       ${h_file} ${cpp_file}   # remove trailing whitespaces
#        sed -i '${/^$/d;}'                      ${h_file} ${cpp_file}   # remove blank line at the end
#        sed -i '1i#include "cmStandardLexer.h"' ${cpp_file}             # add cmStandardLexer.h include
    else
        echo "Skipped generating Parser ${parser} "
    fi
done


popd > /dev/null
